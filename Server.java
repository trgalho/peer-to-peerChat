/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChatP2PSSL;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 *
 * @author João Pedro Bretanha
 */
public class Server 
{
    private SSLSocket socket;
    private SSLServerSocket serverSocket;
    private Certificate serverCertificate;
    private ArrayList<PeerTable> peersOnline;
    private String password;
    
    
    public Server() 
    {   
        Upnp upnp = new Upnp();
        upnp.portMap(6991);
        this.peersOnline = new ArrayList<>();
        this.password = "serverRedes";
        GenCertificate serverCert = new GenCertificate("serverRedes");
        try 
        {   serverCert.geraCertificado("server", this.password);
            //addCertificateKS(serverCertificate, "keystore");
            serverCertificate = serverCert.getCertificate();
            System.setProperty("javax.net.ssl.keyStore","keystore.jks");
            System.setProperty("javax.net.ssl.keyStorePassword", "gustavo");
            //System.setProperty("javax.net.ssl.trustStore", "server.jks");
            //System.setProperty("javax.net.ssl.trustStorePassword",this.password);
            connect();
        } 
        catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | NoSuchProviderException | InvalidKeyException | SignatureException | UnrecoverableKeyException ex) 
        {   ex.printStackTrace();
        } 
    }
    
    public void addCertificateKS(Certificate novo, String alias) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException
    {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, password.toCharArray());
        ks.setCertificateEntry(alias, novo);
        FileOutputStream fos = new FileOutputStream(alias+".jks");
        ks.store(fos, password.toCharArray());
        fos.close();
    }
    
    private void connect() throws IOException
    {        
        SSLServerSocketFactory sslserversocketfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        this.serverSocket = (SSLServerSocket) sslserversocketfactory.createServerSocket(6991);
        byte[] header, message, recevMessage;
        int recevMessageSize;
        
        this.socket = (SSLSocket) serverSocket.accept();
        
        recevMessage = new byte[5000];
        //Recebe a mensagem
        DataInputStream dis = new DataInputStream(this.socket.getInputStream());
        recevMessageSize = dis.read(recevMessage, 0, recevMessage.length);
     
        byte[] temp = new byte[recevMessageSize];
        System.arraycopy(recevMessage, 0, temp, 0, recevMessageSize);
        String messageString = new String(temp);
        System.out.println (messageString);
        
        ExecutorService thread = Executors.newCachedThreadPool();
        thread.execute(new ServerAnswer(temp, peersOnline, socket));
         
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {   
        Server server = new Server();
    }
    
}

