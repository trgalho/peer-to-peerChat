/*
 * To change this license header, choose License Headers requisition Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template requisition the editor.
 */

package peer.to.peerchat;
//////////SEGUNDA PARTE DO TRABALHO///////////
import com.sun.org.apache.bcel.internal.util.ByteSequence;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.Certificate;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
//////////////////////////////////////////////
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket; 
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.print.DocFlavor;
import javax.swing.text.View;

/**
 * 
 * @author trGalho 
 */

public class Server extends Base implements Runnable{
    public static final String KILL_SERVER = "/killserver";
    public static final int TIME_OUT = 2000;
    private Map<String, Object> userAutentication;
    private MessageHandler messageHandler;
    private ServerSocket requisition;
    private MapServer peerList;
    private boolean restart;
    //SSL STUFF + CERTIFICATION STUFF
    private SSLServerSocket sslRequisition;
    private SSLServerSocketFactory sslssf;
    private CertificateFactory certificateFactory;
    private KeyStore dataBase;
    private Certificate certificate;
    ///
    private SSLContext context;
    KeyStore keyStore;
    KeyStore trustStore;
    KeyManagerFactory keyManagerFactory;
    TrustManagerFactory trustManagerFactory;
    InputStream defaultRead;
    OutputStream defaultWrite;
    //
    SSLServerSocket server;
    SSLSocket outputSocket;
    GenCertificate keys;
    public Server() throws IOException, KeyManagementException{
        try{
            //this.requisition = new ServerSocket(Grammar.DEFAULT_SERVER_PORT);
            trustStore =  KeyStore.getInstance("JKS");
            trustStore.load(null, null);
            keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            this.messageHandler = new MessageHandler();
            this.userAutentication = new HashMap<>();
            this.peerList = new MapServer();
            this.restart = false;
            this.context = SSLContext.getInstance("TLS");
            this.keys = new GenCertificate(Grammar.SERVER_ALIAS, Grammar.PASSWORD);
            loadKeyStore();esporte();loadTrustStore();
            context.init(keyManagerFactory.getKeyManagers(),
                    trustManagerFactory.getTrustManagers(), 
                    null);
            server = getSSLServerSocket(Grammar.DEFAULT_SERVER_PORT);
            System.out.println("TOCANDO O ESPORTE PRO CERTIFICADO! VAI TIME");
        } catch (IOException ex){
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException("PORT "+Grammar.DEFAULT_SERVER_PORT+": ALREADY IN USE");
        } catch (KeyStoreException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) throws KeyManagementException{
        try {
            Scanner scanner =  new Scanner(System.in);
            Server masterPeer =  new Server();
            Thread masterPeerThread = new Thread(masterPeer);
            masterPeerThread.start();
            while(!masterPeer.restart() && !scanner.nextLine().equals(KILL_SERVER));
            System.out.println("\n\n DO YOU WANT RESTART SERVER? (Y/N)");
            //if(scanner.nextLine().charAt(0) == 'Y'){
                scanner = null;
                masterPeer=null;
                masterPeerThread=null;
                System.gc();
                Server.main(args);
            //}
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR:\n    "+ex.getMessage());
        }
    }
    
    void connectPeer(String name, String IP) throws SocketException{
        //while(name.charAt(name.length()-1)=='\n'){
        //    name=  name.substring(0, name.length()-1);
        //}
        //if(name.equals("-kill")) this.restart = true;
        peerList.put(name.hashCode(),
                new PeerInfo(name.hashCode(),name, IP,true, null));
        System.out.println("EVENT:\n    PEER "+name+" SUCCESSFULLY CONNECTED!");     
    }
    
    void connectPeerSSL(String IP, Certificate certificado) throws SocketException, CertificateEncodingException, KeyManagementException, KeyStoreException, IOException{
        peerList.put(IP.hashCode(),
                new PeerInfo(IP.hashCode(), IP,certificado.getEncoded().length, certificado));
        addToTrustStore(IP, certificado);
        System.out.println("EVENT:\n    PEER "+IP+" SUCCESSFULLY CONNECTED!");     
    }
    
    void disconnectPeerSSL(String IP) throws SocketException{
        while(IP.charAt(IP.length()-1)=='\n'){
            IP=  IP.substring(0, IP.length()-1);
        }
        //System.out.println("\n--->\t"+peerList.toString());
        PeerInfo p =  peerList.get(IP.hashCode());
        if(p != null){
            peerList.remove(p.id());
           /*remove usa só um parâmetro, tinha 2 ali. p.id() e p*/
            System.out.println("EVENT:\n    PEER "+IP+" SUCCESSFULLY DISCONNECTED!");
            try{
                updateAll();
            } catch (Exception ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                throw new SocketException("SERVER CAN'T SENT UPDATE");
            }
        }else{
            System.out.println("ERROR:\n    NOT CONNECTED PEER "+IP+" TRIED DISCONNECTED!");
        }
    }
    
    
    void disconnectPeer(String name) throws SocketException{
        while(name.charAt(name.length()-1)=='\n'){
            name=  name.substring(0, name.length()-1);
        }
        //System.out.println("\n--->\t"+peerList.toString());
        PeerInfo p =  peerList.get(name.hashCode());
        if(p != null){
            peerList.remove(p.id());
           /*remove usa só um parâmetro, tinha 2 ali. p.id() e p*/
            System.out.println("EVENT:\n    PEER "+name+" SUCCESSFULLY DISCONNECTED!");
            try{
                updateAll();
            } catch (Exception ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                throw new SocketException("SERVER CAN'T SENT UPDATE");
            }
        }else{
            System.out.println("ERROR:\n    NOT CONNECTED PEER "+name+" TRIED DISCONNECTED!");
        }
    }
    
    void updateAllSLL() throws Exception{// throws Exception{
        byte[] encapsulatedMessage = messageHandler.encapsulateRequisitionByte(
                Grammar.MASTER_PEER, Grammar.UPDATE,peerList);
        List<PeerInfo> remove = new ArrayList<>();
        //System.out.println(peerList.toString());
        for(PeerInfo p : peerList.values()){
            if(!sendUpdateSSL(p.ip(), encapsulatedMessage)) 
                remove.add(p);
        }
        for(PeerInfo p : remove){
            peerList.remove(p.id());
            /*remove usa só um parâmetro, tinha 2 ali. p.id() e p*/
        }
        remove = null;
        System.gc();
        remove = null;
        System.gc();
    }
    
    
    void updateAll() throws Exception{// throws Exception{
        String encapsulatedMessage = messageHandler.encapsulateRequisition(
                Grammar.MASTER_PEER, Grammar.UPDATE, peerList.toString());
        List<PeerInfo> remove = new ArrayList<>();
        //System.out.println(peerList.toString());
        for(PeerInfo p : peerList.values()){
            if(!sendUpdate(p.ip(), encapsulatedMessage)) 
                remove.add(p);
        }
        for(PeerInfo p : remove){
            peerList.remove(p.id());
            /*remove usa só um parâmetro, tinha 2 ali. p.id() e p*/
        }
        remove = null;
        System.gc();
    }
    
    public boolean sendUpdateSSL(String IP, byte[] encapsulatedMessage){
        MessageHandler mh;
        try {
            try (SSLSocket update = getSSLSocket(IP, Grammar.DEFAULT_PORT)) {
                System.out.print("\tIP: "+IP);
                update.setSoTimeout(TIME_OUT);
                
                OutputStream output = update.getOutputStream();
                
                InputStream input = update.getInputStream();
                write(output, encapsulatedMessage);
                
                mh = messageHandler.receiveMessageByte(read(input));
                update.close();
                if(mh.getRequest().equals(Grammar.RECV_MSG)){
                    System.out.println("");
                    return true;
                }
                System.out.println("[NOT SENT]");
            }
        } catch (Exception ex) {
            
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;  
    }
    

    
    
    
    
    
    
    
    /**
     *
     * @param IP
     * @param encapsulatedMessage
     * @return
     */
    public boolean sendUpdate(String IP, String encapsulatedMessage){
        MessageHandler mh;
        try {
            try (Socket update = new Socket(IP, Grammar.DEFAULT_PORT)) {
                System.out.print("\tIP: "+IP);
                update.setSoTimeout(TIME_OUT);
                
                DataOutputStream output = new DataOutputStream(update.getOutputStream());
                output.writeUTF(encapsulatedMessage);
                DataInputStream input = new DataInputStream(update.getInputStream());
                mh = messageHandler.receiveMessage(input.readUTF());
                update.close();
                if(mh.getRequest().equals(Grammar.RECV_MSG)){
                    System.out.println("");
                    return true;
                }
                System.out.println("[NOT SENT]");
            }
        } catch (Exception ex) {
            
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;  
    }
    
    
    public void runSEMSSL() {
        Socket response;
        DataInputStream input;
        DataOutputStream output;
        
        MessageHandler mh;
        while(! this.restart){
            try {
                response = requisition.accept();
                response.setSoTimeout(TIME_OUT);
                input = new DataInputStream(response.getInputStream());
                output = new DataOutputStream(response.getOutputStream());
                String aux = input.readUTF();
                System.out.println("\n\n----------------------------------------->"
                                + "\n\t\tRequest:\n"+aux
                                + "$\n<-----------------------------------------\n");
                mh = messageHandler.receiveMessage(aux);
                switch(mh.getOption()){
                    case Grammar.CONNECT:
                        connectPeer(mh.getData(),response.getInetAddress().getHostAddress());
                        try{
                            output.writeUTF(mh.encapsulateRequisition(
                                    Grammar.PEER_GROUP, peerList.toString()));
                        } catch (Exception ex) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                            throw new SocketException("EVENT:\n    SERVER CAN'T SENT RESPONSE");
                        }
                        updateAll();                        
                        break;
                    case Grammar.DISCONNECT:
                        disconnectPeer(mh.getData());
                        break;
                    default: throw new IOException("UNKNOWN REQUEST! "
                            +mh.getRequest()+ " "+mh.getOption());
                }
                response.close();
            } catch (Exception ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("ERROR:\n    "+ex.getMessage());
            }
            
        }
    }
    
    
    
    public boolean restart() {
        return this.restart;
    }
    @Override 
    public void run() {
        SSLSocket response;
        InputStream input;
        OutputStream output;
        
        MessageHandler mh;
        while(! this.restart){
            try {
                response = (SSLSocket) server.accept();
                response.setSoTimeout(TIME_OUT);
                input = response.getInputStream();
                output = response.getOutputStream();
                byte[] aux = read(input);
                System.out.println("\n\n----------------------------------------->"
                                + "\n\t\tRequest:\n"+" "
                                + "$\n<-----------------------------------------\n");
                System.out.println(bytesToString(aux));
                mh = messageHandler.receiveMessageByte(aux);
                switch(mh.getOption()){
                    case Grammar.CONNECT:
                        connectPeerSSL(response.getInetAddress().getHostAddress(),mh.getCertificate());
                        try{
                            write(output, mh.encapsulateRequisitionByte(
                                    Grammar.PEER_GROUP,peerList));
                        } catch (Exception ex) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                            throw new SocketException("EVENT:\n    SERVER CAN'T SENT RESPONSE");
                        }
                        updateAllSLL();                        
                        break;
                    case Grammar.DISCONNECT:
                        disconnectPeer(mh.getData());
                        break;
                    default: throw new IOException("UNKNOWN REQUEST! "
                            +mh.getRequest()+ " "+mh.getOption());
                }
                response.close();
            } catch (Exception ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("ERROR:\n    "+ex.getMessage());
            }
            
        }
    }
    private void sustain(long t){
        long finish = (new Date()).getTime() + t;
        while( (new Date()).getTime() < finish ){}
    }
    
    public void updateSSLServerSocket() throws IOException{
        server.close();
        server = 
          (SSLServerSocket) 
                context.getServerSocketFactory().createServerSocket(
                                                          Grammar.DEFAULT_PORT);
        server.setNeedClientAuth(true);
    }
    
    private SSLSocket getSSLSocket(String IP, int PORT) throws IOException {
        SSLSocketFactory fac = context.getSocketFactory();
        SSLSocket s= (SSLSocket) fac.createSocket(IP, PORT);
        s.setNeedClientAuth(true);
        return s;
         //To change body of generated methods, choose Tools | Templates.
    }
    private SSLServerSocket getSSLServerSocket(int PORT) throws IOException {
        SSLServerSocketFactory fac = context.getServerSocketFactory();
        SSLServerSocket s= (SSLServerSocket) fac.createServerSocket(PORT);
        s.setNeedClientAuth(false);
        return s;
         //To change body of generated methods, choose Tools | Templates.
    }
    public void loadKeyStore() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException{
        keyManagerFactory.init(keys.ks,Grammar.PASSWORD.toCharArray());
    }
    public void loadTrustStore() throws FileNotFoundException, CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException{
        trustStore.load(null, null);
        CertificateFactory cf= CertificateFactory.getInstance("X.509");
        Certificate certificado = cf.generateCertificate(new FileInputStream("server.cer"));
        trustStore.setCertificateEntry(Grammar.SERVER_ALIAS, certificado);
        trustManagerFactory.init(trustStore);
    }
    
    public void addToTrustStore(String alias, Certificate certificado) throws KeyManagementException, KeyStoreException, IOException{
        trustStore.setCertificateEntry(alias, certificado);
        trustManagerFactory.init(trustStore);
        context.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers()
                , null);
    }

    public void esporte() throws IOException, CertificateEncodingException{
        FileOutputStream file = new FileOutputStream("server.cer");
        X509Certificate x =  (X509Certificate) keys.getCertificate();
        System.out.println(x.getSerialNumber());
        file.write(keys.getCertificate().getEncoded());
        file.close();
    }
}

    /*
     *
     * @param args
     * @throws java.io.IOException
     * @throws org.xml.sax.SAXException
     * @throws java.net.SocketException
     * @throws java.net.UnknownHostException
     * @throws javax.xml.parsers.ParserConfigurationException
     *
    
    /*
    **public static void main(String[] args)throws IOException, SAXException,
            SocketException,UnknownHostException, ParserConfigurationException,
            Exception{
        ServerSocket requisition = new ServerSocket(6666);
        MessageHandler msg =  new MessageHandler();
        MapServer peerList =  new MapServer();
        while (true) {
            Socket cliente = requisition.accept();
            System.out.println("*** conexao aceita de (remoto): " +
            cliente.getRemoteSocketAddress());
            System.out.println(cliente.getInetAddress()+":"+cliente.getPort());
            
            DataInputStream ois = new DataInputStream(cliente.getInputStream());
            String aux = ois.readUTF();
            System.out.println(aux);
            msg = msg.receiveMessage(aux);
            System.out.println(msg.getRequest());
            switch(msg.getRequest()){
                case Grammar.MASTER_PEER:
                    switch(msg.getOption()){
                        case Grammar.CONNECT:
                            System.out.println("conectando.....");
                            peerList.put(msg.getData().hashCode(),
                                    new PeerInfo(
                                        msg.getData().hashCode(),
                                        msg.getData(),
                                        cliente.getRemoteSocketAddress().toString(),
                                        true,    
                                        "nada"
                                    )
                                );

                            DataOutputStream outMsg = 
                                    new DataOutputStream(cliente.getOutputStream());
                            aux = peerList.toString();
                            System.out.println(aux +" ===");
                            outMsg.writeUTF(aux);
                            break;
                        case Grammar.DISCONNECT:
                    }
                    break;
                    
            }
            
            
        }
    }*/

    


