/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peer.to.peerchat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import sun.security.x509.CertAndKeyGen;
import javax.net.ssl.*;

/**
 *
 * @author Bruno
 */
public class SSLSocketTest implements Runnable{
    SSLContext context;
    GenCertificate myCert;
    KeyStore keyStore;
    KeyStore trustStore;
    KeyManagerFactory keyManagerFactory;
    TrustManagerFactory trustManagerFactory;
    SSLServerSocketFactory sslServerSocketFactory;
    SSLSocketFactory sslSocketFactory;
    SSLServerSocket sslServerSocket;
    SSLSocket sslSocket;
    String pass = "pass";
    public SSLSocketTest() throws NoSuchAlgorithmException, IOException, KeyManagementException, KeyStoreException, CertificateException, UnrecoverableKeyException, FileNotFoundException, NoSuchProviderException, InvalidKeyException, SignatureException {
        System.setProperty("javax.net.ssl.keyStore", "keyStore.jks");
        System.setProperty("javax.net.ssl.trustStrore", "trustStore.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", Grammar.PASSWORD);

        myCert = new GenCertificate("eu", pass);
        trustStore =  KeyStore.getInstance("JKS");
        trustStore.load(null, null);
        keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        context = SSLContext.getInstance("TLS");
        
        loadKeyStore();exportCer();loadTrustStore();
        context.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), new SecureRandom());
        //context.init(, null, null);
        sslServerSocketFactory=context.getServerSocketFactory();
        sslSocketFactory=context.getSocketFactory();
        sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(Grammar.DEFAULT_PORT);
        sslServerSocket.setNeedClientAuth(true);
    }
    
    public void things() throws IOException, NoSuchAlgorithmException, KeyManagementException{
        sslSocket = (SSLSocket) context.getSocketFactory().createSocket("localhost",Grammar.DEFAULT_PORT);
        
      //  sslSocket.setSSLParameters(context.getSupportedSSLParameters());
        
        sslSocket.setEnabledCipherSuites(sslSocket.getEnabledCipherSuites());
        System.out.println("zueira");
        //taOutputStream d = new DataOutputStream(sslSocket.getOutputStream());
    //  System.out.println(sslSocket.getHandshakeSession().isValid());
        //writeUTF(pass);
        sslSocket.setNeedClientAuth(true);
        
        System.out.println(sslSocket.getSession().getPeerCertificates().length);
        //System.out.println(sslSocket.getHandshakeSession().getLocalCertificates().length);
        sslSocket.getOutputStream().write(pass.getBytes());
        System.out.println("hueheu");
        
    }
    
    public byte[] read(InputStream input) throws IOException{
        byte[] m = new byte[input.read()];
        input.read(m);
        return m;
    };
    public void write(OutputStream output, byte[] m) throws IOException{
        output.write(m.length);
        output.write(m);
    }
    
    @Override
    public void run(){
    
        while(true){
            try {
                sslServerSocket.setNeedClientAuth(true);
                SSLSocket acpt = (SSLSocket) sslServerSocket.accept();
                
                acpt.setNeedClientAuth(true);
                
                
                
                
                
                
                
                byte[] bytes = new byte[4];
                acpt.getInputStream().read(bytes);
                
                System.out.println("rrrrrrrrrr");
                //tring r =d.readUTF();
                System.out.println("bbbbb");
                for(byte b : bytes){
                    System.out.print((char)b);
                }
            } catch (IOException ex) {
                Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void loadKeyStore() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException{
        keyManagerFactory.init(myCert.ks,pass.toCharArray());
        FileOutputStream output = new FileOutputStream("keyStore.jks");
            myCert.ks.store(output, Grammar.PASSWORD.toCharArray());
            output.close();
    }
    public void exportCer() throws IOException, CertificateEncodingException{
        FileOutputStream file = new FileOutputStream("server.cer");
        file.write(myCert.getCertificate().getEncoded());
        file.close();
    }
    public void loadTrustStore() throws FileNotFoundException, CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException{
        trustStore.load(null, null);
        CertificateFactory cf= CertificateFactory.getInstance("X.509");
        Certificate certificado = cf.generateCertificate(new FileInputStream("server.cer"));
        trustStore.setCertificateEntry("server", certificado);
        trustManagerFactory.init(trustStore);
        FileOutputStream output = new FileOutputStream("trustStore.jks");
            trustStore.store(output,Grammar.PASSWORD.toCharArray());
            output.close();
    }
    
    public void addToTrustStore(String alias, Certificate certificado) throws KeyManagementException, KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException{
        trustStore.setCertificateEntry(alias, certificado);
        trustManagerFactory.init(trustStore);
        context.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers()
                , null);
        FileOutputStream output = new FileOutputStream("trustStore.jks");
            trustStore.store(output,Grammar.PASSWORD.toCharArray());
            output.close();
    }
    
    public void updateSSLServerSocket() throws IOException{
        sslServerSocket = 
          (SSLServerSocket) 
                context.getServerSocketFactory().createServerSocket(1995);
    }
    
    public void startSLLSocket(String IP, int PORT) throws IOException{
        sslSocket = 
                (SSLSocket) context.getSocketFactory().createSocket(IP, PORT);
    }
    
    public static void main(String[] args){
        SSLSocketTest ssl;
        try {
            ssl = new SSLSocketTest();
            Thread t = new Thread(ssl);
            t.start();
            ssl.things();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyManagementException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(SSLSocketTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
