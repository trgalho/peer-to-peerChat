package peer.to.peerchat;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GUIClientChat extends javax.swing.JFrame implements Runnable{
    
    boolean isOn = false;
    MessageHandler myHandler;
    Peer myPeer;
    ArrayList<String> usersList;
    List<String> rcvMsg;
    String id = null, msg = null, sndrName = null,
            encapsulatedMessage = null, myName = null, ip = null;
    Thread myThread; 
    
    public GUIClientChat(){
        System.setProperty("javax.net.ssl.keyStore", "keyStore.jks");
        System.setProperty("javax.net.ssl.trustStrore", "trustStore.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", Grammar.PASSWORD);
        myHandler = new MessageHandler();
        usersList = new ArrayList<>();
        rcvMsg = new ArrayList<>();
        myPeer = new Peer();
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        disButton = new javax.swing.JButton();
        sndButton = new javax.swing.JButton();
        clrButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        messageArea = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        usersOnChat = new javax.swing.JTextArea();
        usrLabel = new javax.swing.JLabel();
        usrField = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        sndArea = new javax.swing.JTextArea();
        connectButton = new javax.swing.JButton();
        ipField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        zuera = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(170, 230, 228));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 485));

        disButton.setText("Disconnect");
        disButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disButtonActionPerformed(evt);
            }
        });

        sndButton.setText("Send");
        sndButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sndButtonActionPerformed(evt);
            }
        });

        clrButton.setText("Clear");
        clrButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clrButtonActionPerformed(evt);
            }
        });

        messageArea.setEditable(false);
        messageArea.setColumns(20);
        messageArea.setLineWrap(true);
        messageArea.setRows(5);
        jScrollPane1.setViewportView(messageArea);

        usersOnChat.setEditable(false);
        usersOnChat.setColumns(20);
        usersOnChat.setRows(5);
        jScrollPane2.setViewportView(usersOnChat);

        usrLabel.setText("Username");

        usrField.setToolTipText("");
        usrField.setPreferredSize(new java.awt.Dimension(100, 28));

        sndArea.setColumns(20);
        sndArea.setLineWrap(true);
        sndArea.setRows(5);
        sndArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sndAreaKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(sndArea);

        connectButton.setText("Connect");
        connectButton.setMaximumSize(new java.awt.Dimension(91, 30));
        connectButton.setMinimumSize(new java.awt.Dimension(91, 30));
        connectButton.setPreferredSize(new java.awt.Dimension(91, 30));
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("IP");

        zuera.setBackground(new java.awt.Color(170, 230, 228));
        zuera.setFont(new java.awt.Font("NanumGothic", 2, 8)); // NOI18N
        zuera.setText("GBB Corporation");

        jLabel2.setFont(new java.awt.Font("NanumGothic", 2, 8)); // NOI18N
        jLabel2.setText("Sponsored by PolyStation, Adadis, Nkie, Heimeken");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(usrLabel)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(usrField, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                            .addComponent(ipField, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(connectButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(disButton))
                    .addComponent(zuera, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sndButton, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(clrButton, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(sndButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(clrButton))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(usrLabel)
                            .addComponent(usrField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ipField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(connectButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(disButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(zuera)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addGap(6, 6, 6))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void sndButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sndButtonActionPerformed
        if(isOn){
            try {
                msg = sndArea.getText();
                while(msg.charAt(0)=='\n'){
                    msg=msg.substring(1);
                }
                id = Integer.toString(myPeer.getMyID());
                byte[] teste = myHandler.encapsulateRequisitionByte("SEND_MSG", id, msg);
                messageArea.append(myName+": "+msg+"\n");
                myPeer.sendEncapsulatedMessageBytes(teste);
                   // messageArea.append("Alguns usuários não receberam a mensagem: "
                   // +myPeer.notSentList()+"\n");
                               
                sndArea.setText("");
                List<String> notSent = myPeer.notSentList();
                    if(!notSent.isEmpty()){
                        messageArea.append("Usuários que não receberam a mensagem: ");
                        for (String name : notSent) {
                            messageArea.append(name+" ");
                        }
                        messageArea.append("\n");
                    }
            }catch (Exception ex) {
                Logger.getLogger(GUIClientChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            messageArea.append("You're not connected!\n");
        }
    }//GEN-LAST:event_sndButtonActionPerformed

    private void disButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disButtonActionPerformed
        isOn = false;
        //myThread.interrupt();
        myPeer.disconnect();
        connectButton.setBackground(Color.getColor("f9f9f8"));
        messageArea.append("disconnected from the chat!\n");
        usersOnChat.setText("");
        usersList.clear();
    }//GEN-LAST:event_disButtonActionPerformed

    private void clrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clrButtonActionPerformed
        sndArea.setText("");
    }//GEN-LAST:event_clrButtonActionPerformed

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        myName = usrField.getText();
        ip = ipField.getText();
        if(myName.length() >= 3 && ip.length() >= 9){
            usersList =  myPeer.sslConnect(myName, ipField.getText());
            connectButton.setBackground(Color.green);
            for (String temp : usersList) {
                usersOnChat.append(temp + "\n");
            }
            if(isOn == false){
                isOn = true;
                this.myThread = new Thread(this);
                myThread.start();
            }
        }else if(myName.length() < 3){
            messageArea.append("Your username is too short! Must have at least 3 characters\n");
        }else{
            messageArea.append("Wrong IP\n");
        }
    }//GEN-LAST:event_connectButtonActionPerformed

    private void sndAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sndAreaKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            if(isOn){
                try {
                    msg = sndArea.getText();
                    while(msg.charAt(0)=='\n'){
                        msg=msg.substring(1);
                    }
                    id = Integer.toString(myPeer.getMyID());
                    messageArea.append(myName+": "+msg+"\n");
                    byte[] teste = myHandler.encapsulateRequisitionByte("SEND_MSG", id, msg);
                    myPeer.sendEncapsulatedMessageBytes(teste);
                    sndArea.setText("");
                    List<String> notSent = myPeer.notSentList();
                    if(!notSent.isEmpty()){
                        messageArea.append("Usuários que não receberam a mensagem: ");
                        for (String name : notSent) {
                            messageArea.append(name+" ");
                        }
                        messageArea.append("\n");
                    }
                    
                }catch (Exception ex) {
                    Logger.getLogger(GUIClientChat.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                messageArea.append("You're not connected!\n");
            }
        }
    }//GEN-LAST:event_sndAreaKeyPressed
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clrButton;
    private javax.swing.JButton connectButton;
    private javax.swing.JButton disButton;
    private javax.swing.JTextField ipField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea messageArea;
    private javax.swing.JTextArea sndArea;
    private javax.swing.JButton sndButton;
    private javax.swing.JTextArea usersOnChat;
    private javax.swing.JTextField usrField;
    private javax.swing.JLabel usrLabel;
    private javax.swing.JLabel zuera;
    // End of variables declaration//GEN-END:variables
/*  Fazendo um sacrifício aos deuses nórdicos para que esse Thread funcione 
perfeitamente. */
    @Override
    public void run() {
        while(isOn){
            try{
                try{
                    myPeer.listenSSLServerSocket();
                    //myPeer.sendServerResponse();
                }catch( Exception ex){
                    System.out.print("Exception: "+ex.getMessage()+ex.getCause().getMessage());
                }
                
                if(myPeer.needUpdate()){
                    ArrayList<String> k = myPeer.updateNameList();
                    if( !k.equals(usersList)){
                        usersOnChat.setText("");
                        for (String temp : k){
                            usersOnChat.append(temp + "\n");
                        }
                    }
                }else{
                    rcvMsg = myPeer.nameMessage();
                    if(rcvMsg != null){
                        //messageArea.append("=====> NEW MESSAGE\n");
                        messageArea.append(rcvMsg.get(0)+ ": "+rcvMsg.get(1)+"\n");
                    }
                }
            }catch (Exception ex) {
                Logger.getLogger(GUIClientChat.class.getName()).log(Level.SEVERE, null, ex);
                //System.out.println("\t\tpiroska");
            }
        }
    }
}