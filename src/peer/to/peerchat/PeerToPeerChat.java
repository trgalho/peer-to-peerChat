/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peer.to.peerchat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class PeerToPeerChat {

    
    public static void main(String[] args) throws Exception {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUIClientChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUIClientChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUIClientChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUIClientChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
          System.setProperty("javax.net.ssl.keyStore", "keyStore.jks");
        System.setProperty("javax.net.ssl.trustStrore", "trustStore.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", Grammar.PASSWORD);
//        GenCertificate gc = new GenCertificate("teste","batata");
//        Certificate certificado = gc.getCertificate();
//        
//        MessageHandler mh = new MessageHandler();
//        //String teste = mh.encapsulateRequisition("SEND_MSG", "TESTE", "Uma batata duas batata\n tentesafds as bateata");
//        //byte[] teste = mh.encapsulateRequisitionByte("RECV_MSG", "TESTE QUARTENTA OSOAK AMSA JASL");
//        //String teste = mh.encapsulateRequisition("MASTER_PEER", "CONNECT", "TESTE");
//        byte[] teste = mh.encapsulateRequisitionByte("MASTER_PEER", "CONNECT", certificado);
//        mh = mh.receiveMessageByte(teste);
//        //System.out.println(mh.getCertificate());
//        //Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(teste.get(1)));
//        
//        String message = "192.168.1.250 " + certificado.getEncoded().length + "\n\n";
//        
//        byte messageBytes[] = message.getBytes();
//        byte certificadoTeste[]  = certificado.getEncoded();
//        byte newLine[]      = "\n\n".getBytes();
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
//
//        outputStream.write( "MASTER_PEER UPDATE\n".getBytes() );
//        outputStream.write( messageBytes );
//        outputStream.write( certificadoTeste );
//        outputStream.write( "\n".getBytes() );
//        
//        message = "192.168.1.240 " + certificado.getEncoded().length + "\n\n";
//        messageBytes = message.getBytes();
//        
//        outputStream.write( messageBytes );
//        outputStream.write( certificadoTeste );
//        outputStream.write( "\n".getBytes() );
//        
//        message = "192.168.1.230 " + certificado.getEncoded().length + "\n\n";
//        messageBytes = message.getBytes();
//        
//        outputStream.write( messageBytes );
//        outputStream.write( certificadoTeste );
//        outputStream.write( "\n".getBytes() );
//        
//        message = "192.168.1.220 " + certificado.getEncoded().length + "\n\n";
//        messageBytes = message.getBytes();
//        
//        outputStream.write( messageBytes );
//        outputStream.write( certificadoTeste );
//        outputStream.write( "\n".getBytes() );;;
//       
//        outputStream.write( newLine );
//        
//        byte[] testeTeste = outputStream.toByteArray();
//        
//        mh = mh.receiveMessageByte(testeTeste);
//        
//        System.out.print(mh.getPeerGroup().keySet());
//        List<Integer> list = new ArrayList<>();
//        list.add("192.168.1.220".hashCode());
//        testeTeste = mh.encapsulateRequisitionByte("MASTER_PEER", "UPDATE", list);
//        mh = mh.receiveMessageByte(testeTeste);
//        System.out.print(mh.getPeerGroup().keySet());
        
        //byte[] maisumTeste = mh.encapsulateRequisitionByte("PEER_GROUP", mh.getPeerGroup());
        //mh.receiveMessageByte(maisumTeste);
        
//        mh = mh.receiveMessage("PEER_GROUP\n" +
//                                    "192.168.1.250 "+ testes.length + "\n" +
//                                    testes+"\n" +
//                                    "192.168.1.249 "+ testes.length + "\n" +
//                                    testes.toString()+"\n" +
//                                    "\n\n");
        //String teste = mh.encapsulateRequisition("MASTER_PEER", "UPDATE", mh.getPeerGroup().toString());
        
        //System.out.println(teste);
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIClientChat().setVisible(true);
            }
        });
        
        }
    }
    
