/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package peer.to.peerchat;
///////////SSL ONLY IMPORTS///////////
import static com.sun.org.apache.xerces.internal.util.FeatureState.is;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
///////////////////////////////////////
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.ServerException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.JOptionPane;
import sun.misc.IOUtils;

/**
 * Classe responsável por estabelecer conexões e manipular lista de peers, enviar e receber mensagens.
 * @author trGalho e eDuh :D
 */
public class Peer extends Base{

    private MessageHandler serverMessageHandler;
    private HashMap<Integer, PeerInfo> peerList;
    private ServerSocket waitingForConnection;
    public static final int TIME_OUT = 2000;
    private DataOutputStream serverOutput;
    private MessageHandler messageHandler;
    private DataInputStream serverInput;
    private boolean hasrunListenServer;
    private ArrayList<String> notSent;
    private List<String> nameMessage;
    private DataOutputStream output;
    private DataInputStream input;
    private final int serverPort;
    private boolean needUpdate;
    private String serverIp;
    private String message;
    private Socket server;
    private Socket socket;
    private String name;
    private int myID;
    /**SSL THINGS**/
    private ByteArrayInputStream bInputServer;
    private ByteArrayOutputStream bOutputServer; 
    private ByteArrayInputStream bInput;
    private ByteArrayOutputStream bOutput;
    private byte[] byteMessage;
    private SSLServerSocketFactory sslssf;
    private SSLServerSocket w8nForConnection;
    private SSLSocket sslSocketClient;
    private SSLSocket sslSocketServer;
    private GenCertificate keys;
   /****/
    private SSLContext context;
    KeyStore keyStore;
    KeyStore trustStore;
    KeyManagerFactory keyManagerFactory;
    TrustManagerFactory trustManagerFactory;
    InputStream defaultRead;
    OutputStream defaultWrite;
    /********************/
    /**
     * Construtor da classe Peer.
     * Define a porta padrão do protocolo e inicializa variáveis.
     * 
     */
    public Peer(){
        System.setProperty("javax.net.ssl.keyStore", "keyStore.jks");
        System.setProperty("javax.net.ssl.trustStrore", "trustStore.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", Grammar.PASSWORD);
        this.serverMessageHandler = new MessageHandler();
        this.messageHandler = new MessageHandler();        
        this.serverPort=Grammar.DEFAULT_PORT;
        this.hasrunListenServer = false;
        this.needUpdate = false;
    }
    
    /**
     * Método para obter lista de usuários que não receberam a mensagem.
     * @return Retorna uma lista contendo os nomes de usuário que não receberam a mensagem.
     */
    public List<String> notSentList(){
        return notSent;
    }
    
    /**
     * Retorna um inteiro correspondente ao ID gerado pelo servidor.
     * @return Retorna ID do peer ou 0 (zero) caso não tenha sido possível conectar.
     */
    public int getMyID() {
        return this.myID;
    }
    
    /**
     * Método para obter uma string contendo o nome do peer.
     * @return Retorna name do Peer.
     */
    public String name(){
        return this.name;
    }
    
    /**
     * Método interno para envio de mensagens. Solicita conexão ao peer para envio de uma mensagem.
     * @param ID do peer que irá receber a mensagem.
     */    
    private boolean sendRequest(PeerInfo peer){
        try {
            //System.out.println("\t    "+peer.ip()+" name: "+peer.name());
            startSocket(peer.ip(),serverPort);
            
            this.message =
                    messageHandler.encapsulateRequisition(
                            Grammar.TALK_TO, Integer.toString(myID));
            
            send(this.message);
            String aux = receive();
            messageHandler = messageHandler.receiveMessage(aux);
            this.message = null;
            return validateAnswer(Grammar.ACCEPT_TALKING);            
        } catch (IOException ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    
    /**
     * Método responsável opr enviar a mensagem a todos peers conectados.
     * 
     * @param encapsulatedMessage: mensagem no padrão do protocolo para ser enviada aos peers.
     * @return true, se foi possível enviar a mensagem para todos os peers da lista.
     * false, caso contrário.
     * A lista de usuarios que não receberam a mensagem podem ser obtidas usando o método notSentList().
     */
    public boolean sendEncapsulatedMessage(String encapsulatedMessage){
        boolean sent = true;
        ArrayList<String> cantSend = new ArrayList<>();
        System.out.println(peerList.toString());
        for(PeerInfo p : peerList.values()){
            System.out.print("Writing encapsulate message:\n\t"+encapsulatedMessage);
            if(sendRequest(p)){
                try {
                    send(encapsulatedMessage);
                    String aux = receive();
                    System.out.println("Reading encapsulated message: "+aux);   
                    messageHandler = messageHandler.receiveMessage(aux);
                    if(!validateAnswer(Grammar.RECV_MSG)){
                        sent= false;
                        cantSend.add(p.name());
                    }                    
                }catch (Exception ex) {
                    sent= false;
                    cantSend.add(p.name());
                }finally{
                    
                    try {
                        closeSocket();
                    } catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("CANT CLOSE SOCKET CONNECTION");
                    }
                }
            }else System.out.print("[NOT SENT]");
            System.out.println("");
        }
        //System.gc();
        this.notSent= cantSend;
        return sent; // coloquei isso só para não ficar aparecendo erro
    } 
    
    /**
     * Desconecta peer do servidor.
     * Atribui valor nulo para as variáveis e notifica o Garbage Collector.
     */
    
    
    
    
    
    /**
     * Conecta o usuário ao servidor para obter a lista de peers.
     * @param name: String contendo o nome de usuario escolhido.
     * @param ip: String contendo o ip do servidor.
     * @return Retorna uma lista de strings com os nomes de usuários online.
     * Caso seja impossível conectar, abre uma JOptionPane informando
     * "CONNECTION ERROR" e retorna null.
     */
    public ArrayList<String> connect(String name, String ip){
        try{
            this.serverIp = ip;
            message = 
                messageHandler.encapsulateRequisition(
                        Grammar.MASTER_PEER,Grammar.CONNECT, name);
            
            startSocket(serverIp,Grammar.DEFAULT_SERVER_PORT);
            
            send(message);
            
            this.waitingForConnection = new ServerSocket(Grammar.DEFAULT_PORT);

            messageHandler = messageHandler.receiveMessage(receive());
            if(validateAnswer(Grammar.PEER_GROUP)){
                
                this.name= name;
                this.myID= name.hashCode();
                peerList = messageHandler.getPeerGroup();
                peerList.remove(myID);
                
            }else{
                this.waitingForConnection.close();
                throw new Exception("ERROR: SERVER SENT AN INVALID RESPONSE");
            }
            
            //Gera lista de nomes para a GUI.
            this.message = null;
            return updateNameList();
            //return nameList;
        }catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage()+" "+ex.getClass(),
                    "CONNECTION ERROR",JOptionPane.ERROR_MESSAGE);
            
        }
        return null;
    }
    
    
    
    
    /**
     * Método para atualizar a lista de usuários na GUI.
     * @return Retorna uma lista com os nomes dos usuários online.
     */
    public ArrayList updateNameList(){
        ArrayList<String> nameList = new ArrayList<>();
        //nameList.add(this.name);
        for(PeerInfo p : peerList.values()){
            //nameList.add(p.name());
            nameList.add(p.ip());
        }
        this.needUpdate = false;
        return nameList;
    }
    
    /**
     * Método interno para enviar mensagem usando DataOutputStream.
     * @param message: string contendo a mensagem empacotada.
     * @throws IOException
     */
    private void send(String message) throws IOException{
        output.writeUTF(message);
    }
    /**
     * Gambiarra, pfv Talishow look at this shit.
     * Não sei se devo fazer isso ou não... anw veja isso. 
     * @param message: array de bytes contendo uma mensagem codificada
     * @throws IOException 
     */
    private void send(byte[] message) throws IOException{
        bOutput.write(message);
    }
    /**
     * Método interno para receber mensagem usando DataOutputStream.
     * @return message: string contendo a mensagem empacotada.
     * @throws IOException
     */
    private String receive() throws IOException, InterruptedException{
        return input.readUTF();
    }
    
    /**
     * BROBLEMA AKE;
     * @return
     * @throws IOException
     * @throws InterruptedException 
     */
    private byte[] bReceive() throws IOException, InterruptedException{
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[10000];

        while ((nRead = input.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        
        return buffer.toByteArray();        
    }
    /**
     * Método interno para validar resposta.
     * @return true, se campo request é o esperado. false, caso contrário.
     */
    private boolean validateAnswer(String request){
        return messageHandler.getRequest().equals(request);
    }
    
    /**
     * Abre uma conexão com o endereço especificado com time out de 20 mili.
     * @param ip: ip do host ou servidor para conexão.
     * @param port: porta corresponde ao ip.
     * @throws IOException
     */
    private void startSocket(String ip, int port) throws IOException{
        socket = new Socket(ip, port);
        socket.setSoTimeout(TIME_OUT);
        output = new DataOutputStream(socket.getOutputStream());
        input = new DataInputStream(socket.getInputStream());
    }
    /**
     * Abre uma conexão com o endereço especificado utilizando sockets SSL com
     * TO de 20 milisegundos
     * @param ip : ip do host/servidor para estabelecimento da conexão. 
     * @param port: porta para comunicação.
     * @throws IOException 
     */
    
        
    /**
     * Método para escutar a porta de entrada e receber requisição.
     * @throws java.io.IOException
     */
    public void listenServerSocket() throws IOException, Exception{
        server = waitingForConnection.accept();
        System.out.println("Requisição de conexão aceita....");
        serverInput = new DataInputStream(server.getInputStream());
        serverOutput = new DataOutputStream(server.getOutputStream());
        System.out.println("Lendo mensagem recebida no socket...");
        String aux = serverInput.readUTF();
        System.out.println("\tMensagem recebida pelo ServerSocket:\n\t"+aux);
        serverMessageHandler = serverMessageHandler.receiveMessage(aux);
        System.out.println("Gerando resposta...");
        this.message = generateAnswer(serverInput, serverOutput);
        
        this.hasrunListenServer = this.message !=null;
        serverOutput.writeUTF(this.message);
        System.out.println("Resposta enviada!");
        this.hasrunListenServer =false;
        closeServer();
    }
    
    
    
    
    
    
    /**
     * Método para enviar resposta correspondente a requisição recebida por listenServerSocket().
     * Só poder ser executado após uma excução bem sucedida de listenServerSocket(), caso consrário lança uma exceção.
     * 
     * @throws java.io.IOException
     * @throws java.rmi.ServerException
     */
    public void sendServerResponse() throws IOException, ServerException{
        if(this.hasrunListenServer){
            System.out.println("\tServerResponse\n\t"+this.message);
           
        }else{
            this.message = null;
            this.hasrunListenServer =false;
            closeServer();
            throw new ServerException("This method has a dependency generated by listenServerSocket()");
        }
    }
    
  
    
    /**
     * Método interno para fechar conexão do socket.
     * Fecha a conexão e atribui null a variável socket.
     * @throws IOException
     */
    private void closeSocket() throws IOException{
        if(socket != null){
            this.socket.close();
            this.socket = null;
        }
    }
    private void closeSSLSocketClient() throws IOException{
        if(sslSocketClient != null){
            this.sslSocketClient.close();
            this.sslSocketClient = null;
        }
    }
    
    /**
     * Método interno para fechar conexão aberta pelo SocketServer.
     * Fecha a conexão, atribui null a variável server e notifica o Garbage Collector.
     * @throws IOException
     */
    private void closeServer() throws IOException{
        this.server.close();
        this.server= null;
        //System.gc();
    }
    
    /**
     * Método para obter a mensagem recebida.
     * Index 0: nome do peer que enviou a mensagem.
     * Index 1: corpo da mensagem recebida.
     * @return Lista de strings (ArrayList) ou null.
     */
    public List<String> nameMessage(){
        List<String> aux = this.nameMessage;
        this.nameMessage = null;
        return aux;                
    }
    
    /**
     * Método para checar se é preciso atualizar a lista de peers.
     * @return true: se preciso atualizar. false: caso contrário.
     */
    public boolean needUpdate(){
        return this.needUpdate;
    }
    
    /**
     * Método interno para tratar requisição e gerar resposta no padrão do protocolo.
     * A execução do métod gera os seguintes estados:
     * needUpdate == false && nameMessage == null -> requisicao TALK_TO
     * needUpdate == false && nameMessage != null -> requisicao SEND_MSG
     * needUpdate == true -> requisicao MASTER_PEER UPDATE
     * @param serverMessageHandler, que recebeu a mensagem encapsulada.
     * @return string contendo a mensagem de resposta corresponde e 
     */
    private String generateAnswer(
            DataInputStream serverInput,DataOutputStream serverOutput)
            throws IOException, Exception{
        switch(serverMessageHandler.getRequest()){
            case Grammar.TALK_TO:
                this.nameMessage = null;
                this.needUpdate = false;
                serverOutput.writeUTF(
                        serverMessageHandler.encapsulateRequisition(
                            Grammar.ACCEPT_TALKING, Integer.toString(myID)));
                serverMessageHandler = 
                        serverMessageHandler.receiveMessage(serverInput.readUTF());
                if( !serverMessageHandler.getRequest().equals(Grammar.SEND_MSG)){
                    throw new IOException(
                            "NOT VALID RESPONSE RECEIVED "
                            +serverMessageHandler.getRequest());
                }
            case Grammar.SEND_MSG:
                
                //System.out.println("<"+serverMessageHandler.getOption()+"> <"+serverMessageHandler.getData()+">");
                int id = Integer.parseInt(serverMessageHandler.getOption());
                if(id != myID){
                    PeerInfo info = peerList.get(id);
                    if (info != null) {
                        this.nameMessage = new ArrayList();
                        this.nameMessage.add(info.name());
                        this.nameMessage.add(serverMessageHandler.getData());
                        this.needUpdate = false;
                    }
                }
                
                return serverMessageHandler.encapsulateRequisition(
                        Grammar.RECV_MSG, Integer.toString(myID));
            case Grammar.MASTER_PEER:
                this.peerList = serverMessageHandler.getPeerGroup();
                this.needUpdate = true;
                this.nameMessage = null;
                return serverMessageHandler.encapsulateRequisition(
                        Grammar.RECV_MSG, Integer.toString(myID));
            default: throw 
                    new IOException(
                            "NOT VALID RESPONSE RECEIVED "
                            +serverMessageHandler.getRequest());
        }
    }
    
    
    
    
    
    
    
    
    
    private void sustain(long t){
        long finish = (new Date()).getTime() + t;
        while( (new Date()).getTime() < finish ){}
    }
    public void loadKeyStore() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException{
        keyManagerFactory.init(keys.ks,Grammar.PASSWORD.toCharArray());
    }
    public void loadTrustStore() throws FileNotFoundException, CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException{
        trustStore.load(null, null);
        CertificateFactory cf= CertificateFactory.getInstance("X.509");
        Certificate certificado = cf.generateCertificate(new FileInputStream("server.cer"));
        X509Certificate x = (X509Certificate) certificado;
        System.out.println(x.getSerialNumber());
        trustStore.setCertificateEntry(Grammar.SERVER_ALIAS, certificado);
        trustManagerFactory.init(trustStore);
        FileOutputStream output = new FileOutputStream("trustStore.jks");
            trustStore.store(output,Grammar.PASSWORD.toCharArray());
            output.close();
    }
    
    public void addToTrustStore(String alias, Certificate certificado) throws KeyManagementException, KeyStoreException{
        trustStore.setCertificateEntry(alias, certificado);
        trustManagerFactory.init(trustStore);
        context.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers()
                , null);
    }
    
    public void updateSSLServerSocket() throws IOException{
        w8nForConnection.close();
        w8nForConnection = 
          (SSLServerSocket) 
                context.getServerSocketFactory().createServerSocket(
                                                          Grammar.DEFAULT_PORT);
        w8nForConnection.setNeedClientAuth(true);
    }
    
    public void startSLLSocket__(String IP, int PORT) throws IOException{
        sslSocketClient = 
                (SSLSocket) context.getSocketFactory().createSocket(IP, PORT);
    }
    public void initSSL() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException{
        trustStore =  KeyStore.getInstance("JKS");
        trustStore.load(null, null);
        keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        context = SSLContext.getInstance("TLS");
        loadKeyStore();loadTrustStore();
        context.init(keyManagerFactory.getKeyManagers(),
                     trustManagerFactory.getTrustManagers(),
                     new SecureRandom());
        //context.
    }
    private byte[] generateAnswerSSL(
            InputStream serverInput,OutputStream serverOutput)
            throws IOException, Exception{
        switch(serverMessageHandler.getRequest()){
            case Grammar.TALK_TO:
                this.nameMessage = null;
                this.needUpdate = false;
                write(serverOutput,
                        serverMessageHandler.encapsulateRequisitionByte(
                            Grammar.ACCEPT_TALKING, Integer.toString(myID)));
                System.out.println("\t\t\tread");
                serverMessageHandler = 
                        serverMessageHandler.receiveMessageByte(read(serverInput));
                System.out.println("\t\t\tRead end");
                if( !serverMessageHandler.getRequest().equals(Grammar.SEND_MSG)){
                    throw new IOException(
                            "NOT VALID RESPONSE RECEIVED "
                            +serverMessageHandler.getRequest());
                }
            case Grammar.SEND_MSG:
                
                //System.out.println("<"+serverMessageHandler.getOption()+"> <"+serverMessageHandler.getData()+">");
                int id = Integer.parseInt(serverMessageHandler.getOption());
                if(id != myID){
                    PeerInfo info = peerList.get(id);
                    if (info != null) {
                        this.nameMessage = new ArrayList();
                        this.nameMessage.add(info.name());
                        this.nameMessage.add(serverMessageHandler.getData());
                        this.needUpdate = false;
                    }
                }
                
                return serverMessageHandler.encapsulateRequisitionByte(
                        Grammar.RECV_MSG, Integer.toString(myID));
            case Grammar.MASTER_PEER:
                this.peerList = serverMessageHandler.getPeerGroup();
                for(Integer key : peerList.keySet()){
                    addToTrustStore(String.valueOf(key), peerList.get(key).certificate());
                }
                this.needUpdate = true;
                this.nameMessage = null;
                return serverMessageHandler.encapsulateRequisitionByte(
                        Grammar.RECV_MSG, Integer.toString(myID));
            default: throw 
                    new IOException(
                            "NOT VALID RESPONSE RECEIVED "
                            +serverMessageHandler.getRequest());
        }
    }
    public void disconnect(){
        try {
            startSocket(serverIp,Grammar.DEFAULT_SERVER_PORT);
            this.message = 
                messageHandler.encapsulateRequisition(
                        Grammar.MASTER_PEER,Grammar.DISCONNECT,this.name);
            send(this.message);
        } catch (Exception ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                this.waitingForConnection.close();
            } catch (IOException ex) {
                Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
            }  
                this.message = null;
                this.name = null;
                this.myID = 0;
                
                
                this.waitingForConnection = null;
                
                
                this.socket = null;
                this.input = null;
                this.output = null;
                
                this.server = null;
                this.serverOutput = null;
                this.serverInput = null;
                
                this.nameMessage =null;
                this.notSent = null;
                this.serverIp =null;
                this.peerList =null;
                            
                //System.gc();
        }  
    }
    
    public void sslDisconnect(){
        try {
            startSSLSocket(serverIp,Grammar.DEFAULT_SERVER_PORT);
            
            this.byteMessage = messageHandler.encapsulateRequisitionByte(
                    Grammar.MASTER_PEER, Grammar.DISCONNECT, this.name);
            
            //send(this.byteMessage);
            write(sslSocketClient.getOutputStream(), byteMessage);
        } catch (Exception ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                this.w8nForConnection.close();
            } catch (IOException ex) {
                Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
            }  
                this.byteMessage = null;
                this.name = null;
                this.myID = 0;
                
                
                this.w8nForConnection = null;
                
                
                this.sslSocketClient = null;
                this.bInput = null;
                this.bOutput = null;
                
                this.sslSocketServer = null;
                this.bOutputServer = null;
                this.bInputServer = null;
                
                this.nameMessage =null;
                this.notSent = null;
                this.serverIp =null;
                this.peerList =null;
                            
                //System.gc();
        }  
    }
    /**
     * Mesma coisa acima só que para Sockets SSL geração de keys...(espero).
     * @param name
     * @param ip
     * @return 
     */
    public ArrayList<String> sslConnect(String name, String ip){
        try{
            
            this.serverIp = ip;
            keys = new GenCertificate(name,Grammar.PASSWORD);
            
            //inicializa context para SSL e carrega certificado do servidor
            initSSL();
            this.byteMessage = messageHandler.encapsulateRequisitionByte(
                    Grammar.MASTER_PEER, Grammar.CONNECT, keys.getCertificate());
            startSSLSocket(serverIp,Grammar.DEFAULT_SERVER_PORT);
            
            write(sslSocketClient.getOutputStream(),byteMessage);
            
            
            //usa context para gerar um socketfactory ja com os certificados 
            sslssf = context.getServerSocketFactory();
            
            this.w8nForConnection = (SSLServerSocket) sslssf.
                    createServerSocket(Grammar.DEFAULT_PORT);
            this.w8nForConnection.setReceiveBufferSize(Integer.MAX_VALUE);
            byte [] debug = read(sslSocketClient.getInputStream());
            System.out.println(bytesToString(debug));
            messageHandler = 
               messageHandler.receiveMessageByte(debug);

            //if(validateAnswer(Grammar.PEER_GROUP)){
            if(true){
                this.name= name;
                this.myID= name.hashCode();
                peerList = messageHandler.getPeerGroup();
                peerList.remove(myID);
                
            }else{
                this.w8nForConnection.close();
                throw new Exception("ERROR: SERVER SENT AN INVALID RESPONSE");
            }
            
            //Gera lista de nomes para a GUI.
            this.byteMessage = null;
            return updateNameList();
            //return nameList;
        }catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage()+" "+ex.getClass(),
                    "CONNECTION ERROR",JOptionPane.ERROR_MESSAGE);
            
        }
        return null;
    }
    private void startSSLSocket(String ip, int port) throws IOException{
        SSLSocketFactory sslsf = context.getSocketFactory();
               // (SSLSocketFactory) SSLSocketFactory.getDefault();
        sslSocketClient = (SSLSocket) sslsf.createSocket(ip,port);
        sslSocketClient.setSoTimeout(TIME_OUT);
        sslSocketClient.setSendBufferSize(Integer.MAX_VALUE);
        sslSocketClient.setReceiveBufferSize(Integer.MAX_VALUE);
        output = new DataOutputStream(sslSocketClient.getOutputStream());
        input = new DataInputStream(sslSocketClient.getInputStream());
    }
    
    public void listenSSLServerSocket() throws IOException, Exception{
        server = w8nForConnection.accept();
        System.out.println("Requisição de conexão aceita....");
        InputStream inServer;
        inServer = server.getInputStream();
        OutputStream outServer = server.getOutputStream();
        byte[] aux = read(inServer);
        System.out.println("\tMensagem recebida pelo ServerSocket:\n\t"+bytesToString(aux));
        serverMessageHandler = serverMessageHandler.receiveMessageByte(aux);
        System.out.println("Gerando resposta...");
        aux = generateAnswerSSL(inServer, outServer);
        this.hasrunListenServer = this.message !=null;
        write(outServer, aux);
        System.out.println("Resposta enviada!");
        
        this.hasrunListenServer =false;
        closeServer();
        updateSSLServerSocket();
    }
    private void closeSSLSocket() throws IOException{
        this.sslSocketClient.close();
        this.sslSocketClient = null;
    }
    
      /**
     * Método interno para envio de mensagens com o uso de sockets SSL.
     * @param peer
     * @return 
     */
    private boolean sendSSLRequest(PeerInfo peer){
        try {
            
            startSSLSocket(peer.ip(),serverPort);
            System.out.println("Estabelecendo conexão com peer "+peer.ip());
            this.defaultRead = sslSocketClient.getInputStream();
            this.defaultWrite =  sslSocketClient.getOutputStream();
            this.byteMessage = 
                    messageHandler.encapsulateRequisitionByte(
                            Grammar.TALK_TO, Integer.toBinaryString(myID));
            //write(sslSocketClient.getOutputStream(), byteMessage);
            
            //byte[] aux = read(sslSocketClient.getInputStream());//problem
            
            //messageHandler = messageHandler.receiveMessageByte(aux);
            this.byteMessage = null;
            System.out.println("Validando conexão....");
            return true;//validateAnswer(Grammar.ACCEPT_TALKING);            
        
        } catch (IOException ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Peer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    public boolean sendEncapsulatedMessageBytes(byte[] encapsulatedMessage){
        boolean sent = true;
        ArrayList<String> cantSend = new ArrayList<>();
        for(PeerInfo p : peerList.values()){
            
            if(sendSSLRequest(p)){
                try {
                    System.out.print("\tWriting encapsulate message:\n\t"+bytesToString(encapsulatedMessage));
                    sustain(199);
                    write(sslSocketClient.getOutputStream(),encapsulatedMessage);
                    byte[] aux = read(sslSocketClient.getInputStream());
                    System.out.println("Reading encapsulated message: "+bytesToString(aux));   
                    messageHandler = messageHandler.receiveMessageByte(aux);
                    if(!validateAnswer(Grammar.RECV_MSG)){
                        sent= false;
                        cantSend.add(p.ip());
                    }                    
                }catch (Exception ex) {
                    sent= false;
                    cantSend.add(p.ip());
                }finally{
                    
                    try {
                        closeSSLSocketClient();
                    } catch (IOException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("CANT CLOSE SOCKET CONNECTION");
                    }
                }
            }else System.out.print("[NOT SENT]");
            System.out.println("");
        }
        //System.gc();
        this.notSent= cantSend;
        return sent; // coloquei isso só para não ficar aparecendo erro
    }
}    
    /*
     * METODO PARA VERIFICAR SE OCORREU ALGUMA EXCEÇAO NA EXECUÇAO DO METODO RUN.
     * @return true, caso tenha ocorrido uma excecao. false, caso contrário
     *
    public boolean RunException(){
        return runException;
    } 
    public static void main(String[] args) throws IOException{
        Socket conection = new Socket("localhost",6666);
        DataOutputStream outMsg =  
                new DataOutputStream(conection.getOutputStream());
        DataInputStream inMsg = 
                new DataInputStream(conection.getInputStream());
        
        Scanner scan = new Scanner(System.in);
        String i;
            i = scan.nextLine();            
            outMsg.writeUTF(i+"\n");
            System.out.println(inMsg.readUTF());
            
        
    }
    */