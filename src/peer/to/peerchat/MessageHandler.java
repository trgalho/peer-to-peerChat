/*
 * IMPORTANTE, LEMBRAR:
 * REQUEST = TALK_TO | ACCEPT_TALKING | SEND_MSG | RECV_MSG;
 * OPTION  = UPDATE  | DISCONNECT     | CONNECT  | SIZE(NUMERAL);
 * DATA    = (PEER_ID, PEER_NAME, PEER_IP, PEER_STATUS, PEER_KEY) | PEER_ID |
              MESSAGE_BODY | PEER_NAME;

 * FORMATO: (REQUEST)(OPTION)*(SPACE)(DATA)+(NEWLINE)²;
    ONDE * = 0 OU UMA VEZ;
         + = 1 OU MAIS VEZES;
 */

package peer.to.peerchat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Bruno
 */
public class MessageHandler {
    private String  request;
    private String  option;
    private String  data;
    private HashMap peerGroupH; 
    
    //SSL
    private Certificate certificate;
    private int         certSize;
    
    public MessageHandler(){
        this.request     = new String();
        this.option      = new String();
        this.data        = new String();
        this.peerGroupH  = new HashMap();
    }
       
    /**
     * @param encapsulatedMessage A mensagem encapsulada com cabecalho.
     * @return Um objeto MessageHandler. OBS: Quando utilizado com uma mensagem 
     * SEND_MSG, O PEER_ID é retornado no option do MessageHandler e não em data.
     * Foi feito assim para simplificar a implementação e para não utilizar um
     * ArrayList só pra isso.
     * @throws java.lang.Exception
     */
    public MessageHandler receiveMessage(String encapsulatedMessage) throws Exception {
        MessageHandler  decodifiedMessage = new MessageHandler();
        // Passa para commandList a mensagem em forma de Array separado por SPACE.
        List<String> commandList = Arrays.asList(encapsulatedMessage.split(" "));
        // Passa o item[0] de commandList (requisicao) para o objeto MH.
        decodifiedMessage.setRequest(commandList.get(0));
        
        // Checa se o protocolo esta correto.
        if (checkRequest(decodifiedMessage.getRequest())) {
            switch(decodifiedMessage.getRequest()) {
                case "MASTER_PEER":
                    decodifiedMessage.setOption(commandList.get(1));
                    switch(decodifiedMessage.getOption()) {
                        case "CONNECT":
                            if (commandList.get(2).endsWith("\n\n")) {
                                decodifiedMessage.setData(commandList.get(2).substring(0, commandList.get(2).length() - 2));
                                if (commandList.get(2).endsWith("\n\n\n")) {
                                    throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                                }
                            } else {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                            }
                            break;
                        case "DISCONNECT":
                            decodifiedMessage.setData(commandList.get(2).substring(0, commandList.get(2).length() - 1));
                            if (commandList.get(2).endsWith("\n\n")) {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                            } else if (!commandList.get(2).endsWith("\n")) {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                            }
                            break;
                        case "UPDATE":
                            decodifiedMessage.setPeerGroup(decodePeerList(commandList.get(2)));
                            if (commandList.get(2).endsWith("\n\n\n")) {
                                if (commandList.get(2).endsWith("\n\n\n\n")) {
                                    throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                                }
                            } else {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                            }
                            break;
                        default:
                            throw new Exception("ERRO: PROTOCOLO OPTION INVALIDO, "+decodifiedMessage.getOption());
                    }                  
                    break;
                case "PEER_GROUP":
                    decodifiedMessage.setPeerGroup(decodePeerList(commandList.get(1)));
                    if (commandList.get(1).endsWith("\n\n\n")) {
                        if (commandList.get(1).endsWith("\n\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    break;
                case "SEND_MSG":
                    decodifiedMessage.setOption(commandList.get(2));
                    decodifiedMessage.setData(null);
                    
                    for (int i = 3; i < commandList.size(); i++) {
                        if (decodifiedMessage.getData() != null) {
                            decodifiedMessage.setData(decodifiedMessage.getData() + " " + commandList.get(i));
                        } else {
                            decodifiedMessage.setData(commandList.get(i));
                        }
                    }
                    
                    decodifiedMessage.setData(decodifiedMessage.getData().substring(0, decodifiedMessage.getData().length() - 2));
                    
                    if (encapsulatedMessage.endsWith("\n\n")) {
                        if (encapsulatedMessage.endsWith("\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    //decodifiedMessage.setData(encapsulatedMessage.substring(separatorIndex, Integer.valueOf(commandList.get(1)) + separatorIndex + 1));
                    break;
                default:
                    decodifiedMessage.setData(null);
                    
                    for (int i = 1; i < commandList.size(); i++) {
                        if (decodifiedMessage.getData() != null) {
                            decodifiedMessage.setData(decodifiedMessage.getData() + " " + commandList.get(i));
                        } else {
                            decodifiedMessage.setData(commandList.get(i));
                        }
                    }
                    
                    decodifiedMessage.setData(decodifiedMessage.getData().substring(0, decodifiedMessage.getData().length() - 2));
                    
                    if (encapsulatedMessage.endsWith("\n\n")) {
                        if (encapsulatedMessage.endsWith("\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    break;

            }
        } else {
            //System.out.println(commandList.get(0).substring(0, 10));
            if ("PEER_GROUP".equals(commandList.get(0).substring(0, 10))) {
            //    decodifiedMessage.setPeerGroup(decodePeerListSSL(encapsulatedMessage));
            } else {
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+decodifiedMessage.getRequest());
            }
        } 
       
        
        return decodifiedMessage;
    }
    
    public MessageHandler receiveMessageByte(byte[] byteMessage) throws Exception {
        MessageHandler  decodifiedMessage = new MessageHandler();
        String encapsulatedMessage = new String(byteMessage, "UTF-8");
        //String encapsulatedMessage = new String(byteMessage);
        //System.out.println("OKASKKAK: " + encapsulatedMessage);
        // Passa para commandList a mensagem em forma de Array separado por SPACE.
        List<String> commandList = Arrays.asList(encapsulatedMessage.split(" "));
        // Passa o item[0] de commandList (requisicao) para o objeto MH.
        decodifiedMessage.setRequest(commandList.get(0));
        
        // Checa se o protocolo esta correto.
        if (checkRequest(decodifiedMessage.getRequest())) {
            switch(decodifiedMessage.getRequest()) {
                case "MASTER_PEER":
                    decodifiedMessage.setOption(commandList.get(1));
                    
                    switch(commandList.get(1)) {
                        case "CONNECT":
                            if (encapsulatedMessage.endsWith("\n\n")) {
                                // Pega o tamanho do certificado
                                String[] tempData = commandList.get(2).split("\n");
                                
                                // Seta o tamanho pro objeto
                                decodifiedMessage.setCertSize(Integer.valueOf(tempData[0]));
                                
                                // Utiliza o tamanho do certificado pra criar um novo array de bytes
                                byte[] result = new byte[decodifiedMessage.getCertSize()];
                                
                                // Transfere o conteudo do array original da mensagem para um novo array, onde fica apenas o certificado
                                System.arraycopy(byteMessage, (21 + tempData[0].length()), result, 0, decodifiedMessage.getCertSize());
                                
                                // Restaura o certificado
                                CertificateFactory cf   = CertificateFactory.getInstance("X.509");
                                Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(result));
                                
                                // Seta o certificado para o Objeto
                                decodifiedMessage.setCertificate(certificate);
                                
                                if (commandList.get(2).endsWith("\n\n\n")) {
                                    throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                                }
                            } else {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                            }
                            break;
                        case "DISCONNECT":
                            decodifiedMessage.setData(commandList.get(2).substring(0, commandList.get(2).length() - 1));
                            if (commandList.get(2).endsWith("\n\n")) {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                            } else if (!commandList.get(2).endsWith("\n")) {
                                throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                            }
                            break;
                        default:
                            List<String> tempList = Arrays.asList(commandList.get(1).split("\n"));
                            if (tempList.get(1).equals("OUT:")) {
                                byte[] result = new byte[byteMessage.length - 19];
                                System.arraycopy(byteMessage, 19, result, 0, byteMessage.length - 19);
       
                                decodifiedMessage.setPeerGroup(removePeer(result, this.getPeerGroup()));
                            } else {
                                if (commandList.get(1).substring(0, 6).equals("UPDATE")) {
                                    byte[] result = new byte[byteMessage.length - 19];
                                    System.arraycopy(byteMessage, 19, result, 0, byteMessage.length - 19);

                                    decodifiedMessage.setPeerGroup(decodePeerListSSL(result));
                                } else {
                                    throw new Exception("ERRO: PROTOCOLO OPTION INVALIDO, "+decodifiedMessage.getOption() + " " + commandList.get(1));
                                }
                            }
                    }                  
                    break;
                case "PEER_GROUP":
                    decodifiedMessage.setPeerGroup(decodePeerList(commandList.get(1)));
                    if (commandList.get(1).endsWith("\n\n\n")) {
                        if (commandList.get(1).endsWith("\n\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    break;
                case "SEND_MSG":
                    decodifiedMessage.setOption(commandList.get(2));
                    decodifiedMessage.setData(null);
                    
                    for (int i = 3; i < commandList.size(); i++) {
                        if (decodifiedMessage.getData() != null) {
                            decodifiedMessage.setData(decodifiedMessage.getData() + " " + commandList.get(i));
                        } else {
                            decodifiedMessage.setData(commandList.get(i));
                        }
                    }
                    
                    decodifiedMessage.setData(decodifiedMessage.getData().substring(0, decodifiedMessage.getData().length() - 2));
                    
                    if (encapsulatedMessage.endsWith("\n\n")) {
                        if (encapsulatedMessage.endsWith("\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    //decodifiedMessage.setData(encapsulatedMessage.substring(separatorIndex, Integer.valueOf(commandList.get(1)) + separatorIndex + 1));
                    break;
                default:
                    decodifiedMessage.setData(null);
                    
                    for (int i = 1; i < commandList.size(); i++) {
                        if (decodifiedMessage.getData() != null) {
                            decodifiedMessage.setData(decodifiedMessage.getData() + " " + commandList.get(i));
                        } else {
                            decodifiedMessage.setData(commandList.get(i));
                        }
                    }
                    
                    decodifiedMessage.setData(decodifiedMessage.getData().substring(0, decodifiedMessage.getData().length() - 2));
                    
                    if (encapsulatedMessage.endsWith("\n\n")) {
                        if (encapsulatedMessage.endsWith("\n\n\n")) {
                            throw new Exception("ERRO: PROTOCOLO INVALIDO EXCESSO DE NEWLINEs, "+decodifiedMessage.getOption());
                        }
                    } else {
                        throw new Exception("ERRO: PROTOCOLO INVALIDO AUSENCIA DE NEWLINEs, "+decodifiedMessage.getOption());
                    }
                    break;

            }
        } else {
            //System.out.println(commandList.get(0).substring(0, 10));
            if ("PEER_GROUP".equals(commandList.get(0).substring(0, 10))) {
                byte[] result = new byte[byteMessage.length - 11];
                System.arraycopy(byteMessage, 11, result, 0, byteMessage.length - 11);
                
                decodifiedMessage.setPeerGroup(decodePeerListSSL(result));
            } else {
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+decodifiedMessage.getRequest());
            }
        } 
       
        
        return decodifiedMessage;
    }
    
    /**
     * Recebe uma das Strings definidas em request e um dos parâmetros definidos
     * em data e encapsula a mensagem para envio, se for do tipo MESSAGE_BODY ja
     * codifica para UTF-8.
     * @param request MASTER_PEER | SEND_MSG 
     * @param option UPDATE  | DISCONNECT | PEER_ID
     * @param data PEER_NAME | MESSAGE_BODY = String nao codificado | 
     *              (PEER_ID, PEER_NAME, PEER_IP, PEER_STATUS, PEER_KEY) = String,
     *              utilizar metodo toString da MapServer.
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public String encapsulateRequisition(String request, String option, String data) throws UnsupportedEncodingException, Exception {
        String message = null;
        
        switch(request) {
            case "MASTER_PEER":
                switch(option) {
                    case "CONNECT":
                        message = request + " " + option + " " + data + "\n\n";
                        break;
                    case "DISCONNECT":
                        message = request + " " + option + " " + data + "\n";
                        break;
                    case "UPDATE":
                        message = request + " " + option + " " + data + "\n\n";
                        break;
                    default:
                        throw new Exception("ERRO: PROTOCOLO OPTION INVALIDO, "+option);
                }   
                break;
            case "SEND_MSG":
                try {//teste
                    String utfText = new String(data.getBytes("UTF-8"));
                    
                    message = request + " " + utfText.length() + " " + option + " " + utfText + "\n\n";
                } catch (UnsupportedEncodingException e) {
                    System.out.println(e.getMessage());
                }
                break;
            default:
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
        }
      
        return message;
    }
    
    /**
     * Recebe uma das Strings definidas em request e um dos parâmetros definidos
     * em data e encapsula a mensagem para envio, se for do tipo MESSAGE_BODY ja
     * codifica para UTF-8.
     * @param request MASTER_PEER | SEND_MSG 
     * @param option DISCONNECT | PEER_ID
     * @param data PEER_NAME | MESSAGE_BODY = String nao codificado | 
     *              (PEER_ID, PEER_NAME, PEER_IP, PEER_STATUS, PEER_KEY) = String,
     *              utilizar metodo toString da MapServer.
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public byte[] encapsulateRequisitionByte(String request, String option, String data) throws UnsupportedEncodingException, Exception {
        String message = null;
        byte protocol[] = null;
        
        switch(request) {
            case "MASTER_PEER":
                switch(option) {
                    case "DISCONNECT":
                        message = request + " " + option + " " + data + "\n";
                        break;
                    case "UPDATE":
                        message = request + " " + option + " " + data + "\n\n";
                        break;
                    default:
                        throw new Exception("ERRO: PROTOCOLO OPTION INVALIDO, "+option);
                }   
                break;
            case "SEND_MSG":
                try {//teste
                    String utfText = new String(data.getBytes("UTF-8"));
                    
                    message = request + " " + utfText.length() + " " + option + " " + utfText + "\n\n";
                } catch (UnsupportedEncodingException e) {
                    System.out.println(e.getMessage());
                }
                break;
            default:
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
        }
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        
        outputStream.write(message.getBytes());
        protocol = outputStream.toByteArray();
        
        return protocol;
    }
    
    
     /**
     * Metodo utilizado para a nova versão do protocolo
     * @param request MASTER_PEER
     * @param option CONNECT
     * @param certSize Tamanho do Certificado - definido pelo protocolo.
     * @param data Certificado do Peer.
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    
    public byte[] encapsulateRequisitionByte(String request, String option, Certificate data) throws UnsupportedEncodingException, Exception {        
        String message = null;
        byte protocol[] = null;
        int certSize = data.getEncoded().length;
        
        switch(request) {
            case "MASTER_PEER":
                switch(option) {
                    case "CONNECT":
                        message = request + " " + option + " " + certSize + "\n";
                        
                        byte messageBytes[] = message.getBytes();
                        byte certificado[]  = data.getEncoded();
                        byte newLine[]      = "\n\n".getBytes();

                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );

                        outputStream.write( messageBytes );
                        outputStream.write( certificado );
                        outputStream.write( newLine );

                        protocol = outputStream.toByteArray();
                        break;
                    default:
                        throw new Exception("ERRO: PROTOCOLO OPTION INVALIDO, "+option);
                }   
                break;
            default:
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
        }
      
        return protocol;
    }
    
    /**
     * Envia o grupo dos que saíram. Isso aqui tá mt confuso graças a esse protocolo show
     * @param request MASTER_PEER
     * @param option UPDATE
     * @param peerID Array dos peerID dos que sairam
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public byte[] encapsulateRequisitionByte(String request, String option, List<Integer> peerID) throws UnsupportedEncodingException, Exception {
        String message = null;
        byte protocol[] = null;
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        
        switch(request) {
            case "MASTER_PEER":
                switch(option) {
                    case "UPDATE":
                        message = request + " " + option + "\n";
                        outputStream.write(message.getBytes());
                        for (int i = 0; i < peerID.size(); i++) {
                            message = "OUT:\n" + peerID.get(i) + "\n";
                            outputStream.write(message.getBytes());
                        } 
                        outputStream.write("\n\n".getBytes());
                        break;
                }   
                break;
            default:
                throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
        }
        
        
        protocol = outputStream.toByteArray();
        
        return protocol;
    }
    
     /**
   
   * Recebe uma das Strings definidas em request e um dos parâmetros definidos
      * em data e encapsula a mensagem para envio.
     * @param request PEER_GROUP | ACCEPT_TALKING | RECV_MSG | TALK_TO
     * @param data PEER_ID | 
     *              (PEER_ID, PEER_NAME, PEER_IP, PEER_STATUS, PEER_KEY) = String,
     *              utilizar metodo toString da MapServer.
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public String encapsulateRequisition(String request, String data) throws Exception {
        String message = null;
      
        switch(request) {
            case "PEER_GROUP":
                message = request + " " + data + "\n\n";
                break;
            default:
                if (checkRequest(request)) {
                    message = request + " " + data + "\n\n";
                } else {
                    throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
                }
        }
      
        return message;
    }
    
    /**
      * Recebe uma das Strings definidas em request e um dos parâmetros definidos
      * em data e encapsula a mensagem para envio.
     * @param request ACCEPT_TALKING | RECV_MSG | TALK_TO
     * @param data MENSAGEM
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public byte[] encapsulateRequisitionByte(String request, String data) throws Exception {
        String message = null;
        byte[] protocol = null;
        
        switch(request) {
            default:
                if (checkRequest(request)) {
                    message = request + " " + data + "\n\n";
                } else {
                    throw new Exception("ERRO: PROTOCOLO REQUEST INVALIDO, "+request);
                }
        }
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        
        outputStream.write(message.getBytes());
        protocol = outputStream.toByteArray();
      
        return protocol;
    }
         
    /**
      * Recebe uma das Strings definidas em request e um dos parâmetros definidos
      * em data e encapsula a mensagem para envio.
     * @param request PEER_GROUP
     * @param peerGroup
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public byte[] encapsulateRequisitionByte(String request, HashMap peerGroup) throws Exception {
        String message = null;
        byte[] protocol = null;
      
        HashMap<Integer, PeerInfo> peerList = peerGroup;
        
        switch(request) {
            case "PEER_GROUP":
                message = request + "\n";
                
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
                outputStream.write(message.getBytes());
                
                for(PeerInfo p : peerList.values()){
                    message = p.ip() + " " + p.certSize() + "\n\n";
                    outputStream.write(message.getBytes());
                    outputStream.write(p.certificate().getEncoded());
                    outputStream.write("\n".getBytes());
                }
                outputStream.write("\n\n".getBytes());
                //message = request + "\n" + peerIP + " " + certSize + "\n\n" + certificate + "\n\n";
                protocol = outputStream.toByteArray();
                break;
        }
      
        return protocol;
    }
    
    
    /**
      * Recebe uma das Strings definidas em request e um dos parâmetros definidos
      * em data e encapsula a mensagem para envio.
     * @param request MASTER_PEER
     * @param option UPDATE
     * @param peerGroup HashMap
     * @return A mensagem encapsulada.
     * @throws UnsupportedEncodingException
     */
    public byte[] encapsulateRequisitionByte(String request, String option, HashMap peerGroup) throws Exception {
        String message = null;
        byte[] protocol = null;
      
        HashMap<Integer, PeerInfo> peerList = peerGroup;
        
        switch(request) {
            case "MASTER_PEER":
                if (option.equals("UPDATE"))
                message = request + " " + option + "\n";
                
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
                outputStream.write(message.getBytes());
                
                for(PeerInfo p : peerList.values()){
                    message = p.ip() + " " + p.certSize() + "\n\n";
                    outputStream.write(message.getBytes());
                    outputStream.write(p.certificate().getEncoded());
                    outputStream.write("\n".getBytes());
                }
                outputStream.write("\n\n".getBytes());
                //message = request + "\n" + peerIP + " " + certSize + "\n\n" + certificate + "\n\n";
                protocol = outputStream.toByteArray();
                break;
        }
      
        return protocol;
    }
    
    public boolean checkRequest(String request) {
        switch(request){
            case "MASTER_PEER":
                return true;
            case "PEER_GROUP":
                return true;
            case "SEND_MSG":
                return true;
            case "ACCEPT_TALKING":
                return true;
            case "RECV_MSG":
                return true;
            case "TALK_TO":
                return true;
            default:
                return false;
        }
    }
   
    /**
     * METODO IMPLEMENTADO.
     * @param tempData String com a lista de Peers, no formato (PEER_ID, PEER_NAME, PEER_IP, PEER_STATUS, PEER_KEY) + NEWLINE
     * @return Um HashMap dos Peers. Onde key é um Integer e value um objeto PeerInfo.
     */
    private HashMap decodePeerList(String tempData) {
        /* throw new UnsupportedOperationException("Talisson tem que implementar essa naba");
            Implementei essa naba!
        */
        // MAGIC DONT TOUCH!

        System.out.println(tempData);
        List<String> list = Arrays.asList(tempData.split("\n"));
        HashMap<Integer, PeerInfo>  peerGroup = new MapServer();
        for(String info : list){
            System.out.println(info);
            info = info.substring(1, info.length() -1);
            String[] peerInfo = info.split(",");
            Integer key = Integer.parseInt(peerInfo[0]);
            peerGroup.put(key, 
                new PeerInfo(
                    key,
                    peerInfo[1],
                    peerInfo[2],
                    peerInfo[3].equals(Grammar.PEER_STATUS.ONLINE),
                    peerInfo[4]
                )
            );           
        }
        
       
        return peerGroup;
    }
    
    private HashMap decodePeerListSSL(byte[] tempData) throws CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        /* throw new UnsupportedOperationException("Talisson tem que implementar essa naba");
            Implementei essa naba!
        */
        // MAGIC DONT TOUCH!
        byte[] tempTeste = new byte[tempData.length - 2];
        tempTeste = tempData;
        HashMap<Integer, PeerInfo>  peerGroup = new MapServer();
        int j = 1;
        
        do {
            String encapsulatedMessage = new String(tempTeste, "UTF-8");         
            List<String> list = Arrays.asList(encapsulatedMessage.split("\n"));
            
            String[] peerInfo = list.get(0).split(" ");
            String ip = peerInfo[0];
            int certSize = Integer.valueOf(peerInfo[1]);
            
            int tempLenght = ip.length() + peerInfo[1].length() + 3;
            
            byte[] certificateByte = new byte[certSize];
            
            System.arraycopy(tempTeste, tempLenght, certificateByte, 0, certSize);
                                
            // Restaura o certificado
            CertificateFactory cf   = CertificateFactory.getInstance("X.509");
            Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(certificateByte));

            tempTeste = new byte[tempTeste.length - (tempLenght + certSize) - 1];

            System.arraycopy(tempData, (tempLenght + certSize + 1) * j, tempTeste, 0, tempTeste.length);
      
            peerGroup.put(ip.hashCode(), 
                new PeerInfo(
                    ip.hashCode(),
                    ip,
                    certSize,
                    certificate
                )
            );
           
            j++;
        } while (tempTeste.length > 50);
        
        return peerGroup;
    }
    
    private HashMap removePeer(byte[] tempData, HashMap peerGroup) throws CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        /* throw new UnsupportedOperationException("Talisson tem que implementar essa naba");
            Implementei essa naba!
        */
        // MAGIC DONT TOUCH!
        String encapsulatedMessage = new String(tempData, "UTF-8");         
        List<String> list = Arrays.asList(encapsulatedMessage.split("\n"));
        
        for(int i = 0; i < list.size(); i++) {
            if (!list.get(i).equals("OUT:")) {
                peerGroup.remove(list.get(i));
            }
        }
            
        return peerGroup;
    }
    
    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getData() {
        return data;
    }
    
    public void setData(String data) {
        this.data = (data);
    }

    public HashMap getPeerGroup() {
        return peerGroupH;
    }

    public void setPeerGroup(HashMap peerGroup) {
        this.peerGroupH = peerGroup;
    }
    
    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public int getCertSize() {
        return certSize;
    }

    public void setCertSize(int certSize) {
        this.certSize = certSize;
    }

}
