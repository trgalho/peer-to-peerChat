/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peer.to.peerchat;

import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 *
 * @author eDuh
 */
public class SSLServer 
{
    private SSLSocket socket;
    private SSLServerSocket serverSocket;
    private Certificate serverCertificate;
    private ArrayList<PeerInfo> peersOnline;
    private String password;
    
    
    public SSLServer() throws KeyStoreException, IOException, FileNotFoundException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException, UnrecoverableKeyException 
    {   
//        Upnp upnp = new Upnp();
//        upnp.portMap(6991);
        this.peersOnline = new ArrayList<>();;
        this.password = "sslServer";
        GenCertificate serverCert = new GenCertificate("serverRedes", this.password);
        //try 
        //{   
            //addCertificateKS(serverCertificate, "keystore");
            serverCertificate = serverCert.getCertificate();
            System.setProperty("javax.net.ssl.keyStore","keystore.jks");
            System.setProperty("javax.net.ssl.keyStorePassword", "gustavo");
            //System.setProperty("javax.net.ssl.trustStore", "server.jks");
            //System.setProperty("javax.net.ssl.trustStorePassword",this.password);
            connect();
        //} 
    }
    
    public void addCertificateKS(Certificate novo, String alias) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException
    {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, password.toCharArray());
        ks.setCertificateEntry(alias, novo);
        FileOutputStream fos = new FileOutputStream(alias+".jks");
        ks.store(fos, password.toCharArray());
        fos.close();
    }
    
    private void connect() throws IOException
    {        
        SSLServerSocketFactory sslserversocketfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        this.serverSocket = (SSLServerSocket) sslserversocketfactory.createServerSocket(6991);
        byte[] header, message, recevMessage;
        int recevMessageSize;
        
        this.socket = (SSLSocket) serverSocket.accept();
        
        recevMessage = new byte[5000];
        //Recebe a mensagem
        DataInputStream dis = new DataInputStream(this.socket.getInputStream());
        recevMessageSize = dis.read(recevMessage, 0, recevMessage.length);
     
        byte[] temp = new byte[recevMessageSize];
        System.arraycopy(recevMessage, 0, temp, 0, recevMessageSize);
        String messageString = new String(temp);
        System.out.println (messageString);
        
        ExecutorService thread = Executors.newCachedThreadPool();
         
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws KeyStoreException, IOException, FileNotFoundException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, UnrecoverableKeyException, InvalidKeyException 
    {   
       // SSLServer server = new SSLServer();
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (i!= toInt(toBytes(i))){
                System.out.println("ERRO!");
                System.out.println(i);
                System.out.println(toInt(toBytes(i)));
                break;
            }
        }
    }
    static byte[] toBytes(int i){
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result;
    }
    static int toInt(byte[] b){
        int result= (b[0] & 0xff);
        for (int i = 1; i < 4; i++) {
            result = result<<8;
            result+=(b[i] & 0xff);
        }
        return result;
    }
    
}

