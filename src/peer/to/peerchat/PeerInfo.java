/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package peer.to.peerchat;

import java.security.cert.Certificate;

/**
 * Classe destinada a armazenar informações a respeito do peer.
 * @author Bruno
 */
public class PeerInfo {
    // inicio modificacoes 
    private int id;
    private String name;
    private String ip;
    private boolean status;
    private String key;
    
    // adicionado para o SSL
    private int certSize;
    private Certificate certificate;
    
    public PeerInfo(int id,String name, String ip,boolean status, String key){
        this.id=id;
        this.name=name;
        this.ip=ip;
        this.status=status;
        this.key=key;
    }
    
    public PeerInfo(int id, String ip, int certSize, Certificate certificate){
        this.id=id;
        this.ip=ip;
        this.certSize=certSize;
        this.certificate=certificate;
    }
    
    public int id(){
        return id;
    }
    
    public String name(){
        return name;
    }
    public String ip(){
        return ip;
    }
    public int certSize(){
        return certSize;
    }
    
    public Certificate certificate(){
        return certificate;
    }
    
    public String statusString(){
        return status ? Grammar.PEER_STATUS.ONLINE : Grammar.PEER_STATUS.OFFLINE;
    }
    public String getKey(){
        return key;
    }
    //fim modificacoes
    
    // [get|set]PeerID mantido pois PeerID nao foi removido ou substituido
    

    
    
    
    
    /*  Metodos obsoletos. set<Atributo> agora eh realizado pelo construtor.
    **  Atribuitos substituidos (renomeados).     
    **
    public void setPeerID(int peerID) {
        this.peerID = peerID;
    }
    
    public String getPeerName() {
        return peerName;
    }

    public void setPeerName(String peerName) {
        this.peerName = peerName;
    }

    public String getPeerIP() {
        return peerIP;
    }

    public void setPeerIP(String peerIP) {
        this.peerIP = peerIP;
    }

    public void setPeerStatus(boolean peerStatus) {
        this.peerStatus = peerStatus;
    }

    public String getPeerKey() {
        return peerKey;
    }

    public void setPeerKey(String peerKey) {
        this.peerKey = peerKey;
    }
    /**/
}
