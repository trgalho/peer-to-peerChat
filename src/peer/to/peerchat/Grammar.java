/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package peer.to.peerchat;

/**
 * Classe abstrata contendo strings de uso recorrente no protocolo e um inteiro com a porta padrão do protocolo.
 * Todos os atributos são final, implicando que pode ser utilizados como case: dentro de um switch().
 * @author trGalho 
 */
public abstract class Grammar {
    /**
     * Classe estática contendo strings que representam o status do peer.
     * 
     */
    public abstract static class PEER_STATUS{        
        public static final String OFFLINE = "OFFLINE";
        public static final String ONLINE = "ONLINE";
    }
    //public static String NEWLINE = "\n";
    //public static String SPACE = " ";
    public static final String MASTER_PEER = "MASTER_PEER";
    public static final String CONNECT = "CONNECT";
    public static final String DISCONNECT = "DISCONNECT";
    public static final String UPDATE = "UPDATE";
    public static final String SEND_MSG = "SEND_MSG";
    public static final String RECV_MSG = "RECV_MSG";
    public static final String PEER_GROUP = "PEER_GROUP";
    public static final String TALK_TO = "TALK_TO";
    public static final String ACCEPT_TALKING = "ACCEPT_TALKING";
    public static final String PASSWORD = "pass";
    public static final String SERVER_ALIAS = "__server__";
    public static final int DEFAULT_PORT = 6991;
    public static final int DEFAULT_SERVER_PORT = 1995;
    public static final int DEFAULT_KEYSIZE = 1024;
    
    
}
