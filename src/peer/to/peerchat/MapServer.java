/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package peer.to.peerchat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author trGalho
 
 */
public class MapServer extends HashMap<Integer, PeerInfo>{
    @Override public String toString(){
        String peerList=new String();
        for(int PEER_ID : keySet()){
            PeerInfo peer = get(PEER_ID);
            peerList+= "("+ PEER_ID +','+ peer.name() +','+peer.ip()
                    +  ','+ peer.statusString() +','+peer.getKey()+')'+'\n';
        }
        System.gc();
        return peerList;
    }
    /**
     * Método para testes.
     * @param args nem uso essa bagaça.
    */
    public static void main(String[] args){/*/
        String peerGroup = "(1,fulano,192.168.0.1:999,ONLINE,abcedf)\n"
                + "(2,fulano,192.168.0.1:999,ONLINE,xyz)\n"
                + "(3,fulano,192.168.0.1:999,OFFLINE,jklm)\n"
                + "\n\n";
        List<String> list = Arrays.asList(tempData.split("\n"));
            HashMap<Integer, PeerInfo>  peerGroup = new MapServer();
            for(String info : list){
                info= info.substring(1, info.length() -1);
                String[] peerInfo = info.split(",");
                Integer key = Integer.parseInt(peerInfo[0]);
                peerGroup.put(
                    key, 
                    new PeerInfo(
                        peerInfo[1],
                        peerInfo[2],
                        peerInfo[3].equals(Grammar.PEER_STATUS.ONLINE),
                        peerInfo[4]
                    )
                );           
            }

        /**/
        }
}
