/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peer.to.peerchat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Bruno
 */
public class Base {
    public byte[] read(InputStream input) throws IOException{
        byte[] integerBytes = new byte[4];
        input.read(integerBytes);
              
        byte[] m = new byte[toInt(integerBytes)];
        System.out.println("\tTAMANHO DA MENSAGEM RECEBIDA: "+toInt(integerBytes));
        for(int i=0;i<m.length;i++){
            m[i] = (byte) input.read();
        }
        System.out.println("\tMENSAGEM RECEBIDA:   ");
            
        System.out.println(bytesToString(m));
        
        return m;
    };
     public void write(OutputStream output, byte[] m) throws IOException{
        System.out.println("TAMANHO DA MENSAGEM ENVIADA: "+m.length);
        output.write(toBytes(m.length));
        System.out.println("\tMENSAGEM ENVIADA:   ");
        System.out.println(bytesToString(m));
        output.write(m,0,m.length);
    }
    public String bytesToString(byte[] m){
        String s = new String();
        for(byte b: m) s+=(char)b;
        return s;
    }
    byte[] toBytes(int i){
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);
        for(byte b: result) System.out.println(Integer.toBinaryString(b));
        return result;
    }
    int toInt(byte[] b){
        int result= (b[0] & 0xff);
        //System.out.println("result:\t\t\t\t"+Integer.toBinaryString(b[0]));
        for (int i = 1; i < 4; i++) {
            result = result<<8;
            result+=(b[i] & 0xff);
            //System.out.println("result:\t\t\t\t"+Integer.toBinaryString(b[i]));
        }
        return result;
    }
    
}
